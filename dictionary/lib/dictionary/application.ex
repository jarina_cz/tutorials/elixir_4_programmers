defmodule Dictionary.Application do

  alias Dictionary.WordList

  use Application

  def start(_type, _args) do

    import Supervisor.Spec

    children = [
      worker(WordList, [])
    ]

    options = [
      name: Dictionary.Supervisor,
      strategy: :one_for_one
    ]

    Supervisor.start_link(children, options)
  end

end
